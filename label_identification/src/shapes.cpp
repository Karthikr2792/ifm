#include "shapes.hpp"

using namespace IFM::Identification;

vector<cv::Vec4i> Shapes::shapeLines(Image data)
{
    cv::HoughLinesP(data, this->lines, 1, CV_PI/180, 80, 100, 10);

    return this->lines;

}
