#include "process_images.hpp"

using namespace IFM::Identification;


ProcessImages::ProcessImages(Image data)
{
    this->f = data;
}

Image ProcessImages::resizer(Image data)
{
    /*
     * Makes a resize without aspect ratio distortion.
     * Returns image with 1/8th the size of actual image.
     * Reduced size to make processing faster.
     *
     * param: cv::Mat or Image as typedef modified
     *
     * return: Image
     */
   cv::resize(data, this->f, cv::Size(), 0.125, 0.125, CV_INTER_AREA);
   return this->f;
}


Image ProcessImages::colorToGray(Image data)
{
    /*
     * Converts RGB to gray for easier identification of labels.
     * Assumption made that all labels are white.
     *
     * param: Image
     *
     * return: Image
     */
    cv::cvtColor(data, this->f, cv::COLOR_RGB2GRAY);
    return this->f;
}

Image ProcessImages::smoothen(Image data)
{
    /*
     * Gaussian Blur added to picture to obtain clean edges
     *
     * Param: Image
     *
     * return: Image
     */
    cv::GaussianBlur(data,this->f,cv::Size(3,3),0,0);
    return this->f;
}

Image ProcessImages::obtainEdges(Image data)
{
    /*
     * Canny edge used for a clean image with lower Threshold 150 and upper Threshold 175
     *
     * Param: Image
     *
     * return: Image
     */


    //cv::Canny(data, this->f,150,175);
    //cv::adaptiveThreshold(data, this->f,255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY_INV,15, 7);
    //cv::bitwise_not(this->f, this->f);
    cv::Canny(this->f, this->f,20,100);

    return this->f;
}

Image ProcessImages::fillHoles(Image data)
{
    /*
     * Uses Flood fill to remove unclosed areas.
     * Use only when all labels have a closed area as output after edge detection
     * Or there is risk of losing data on labels.
     *
     * Param: Image
     *
     * return: Image
     */
    //cv::floodFill(data, cv::Point(0,0), cv::Scalar(255));
    cv::dilate(data, this->f, (5,5));
    cv::morphologyEx(data, this->f,cv::MORPH_CLOSE,(5,5));
    cv::floodFill(this->f, cv::Point(0,0), cv::Scalar(255));
    //cv::bitwise_not(data, this->f);
    return this->f;
}
