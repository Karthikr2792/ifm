#ifndef PROCESS_IMAGES_HPP
#define PROCESS_IMAGES_HPP

#include "types.hpp"

namespace IFM { namespace Identification {

/*
 * Contains basic scaling and morphological operations
 */

class ProcessImages{
    Image f;
public:
    ProcessImages(Image);
    ~ProcessImages(){};

    Image resizer(Image);

    Image colorToGray(Image);

    Image smoothen(Image);

    Image obtainEdges(Image);

    Image fillHoles(Image);
};

}

}

#endif
