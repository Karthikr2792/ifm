#ifndef SHAPES_HPP
#define SHAPES_HPP

#include "types.hpp"

namespace IFM { namespace Identification {

class Shapes {
    Shapes();
    ~Shapes(){}
public:

    Image f;

    vector<cv::Vec4i> lines;

    vector<cv::Vec4i> shapeLines(Image);
};

}
}

#endif
