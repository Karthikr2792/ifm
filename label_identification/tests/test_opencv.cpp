#include<iostream>
#include<opencv2/opencv.hpp>
#include "types.hpp"
#include "process_images.hpp"

using namespace std;
using namespace cv;
using namespace IFM::Identification;

int main(int argc, char** argv)

{
    Image f;
    Image output;

    f = imread("/home/karthik/IFM/images/506.jpg");
    ProcessImages processor = ProcessImages(f);

    Image g = processor.resizer(f);
    g = processor.colorToGray(g);
    bitwise_not(g,g);
    g = processor.smoothen(g);
    g = processor.fillHoles(g);
    /*namedWindow("Before thresh", WINDOW_AUTOSIZE);

    cv::imshow("Before thresh", g);
    double thresh = threshold(g,g,100, 255, CV_THRESH_BINARY_INV);
    */
    //namedWindow("Before edge", WINDOW_AUTOSIZE);

    //cv::imshow("Before edge", g);
    //g = processor.obtainEdges(g);
    vector<Vec4i> lines;
        HoughLinesP( g, lines, 1, CV_PI/180, 150, 15, 10 );
        for( size_t i = 0; i < lines.size(); i++ )
        {
            line( g, Point(lines[i][0], lines[i][1]),
                Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 3, 8 );
        }
    namedWindow("Display Image", WINDOW_AUTOSIZE);

    cv::imshow("Display Image", g);

    waitKey(0);

    return 0;


}


//cv::Laplacian(g, output, CV_64F, 1,1 ,0, BORDER_DEFAULT);
